import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainpageComponent } from './mainpage/mainpage.component';
import { ContentComponent } from './content/content.component';
import { ContentPageTwoComponent } from './content-page-two/content-page-two.component';


const routes: Routes = [
  {
    path: '', component: MainpageComponent, children: [
      { path: 'content', component: ContentComponent },
      { path: 'content-two', component: ContentPageTwoComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
