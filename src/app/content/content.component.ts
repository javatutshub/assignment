import { Component, OnInit } from '@angular/core';
import { WheatherDataService } from '../service/wheather-data.service';
import { resolve } from 'url';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  public data:any=[];
  cityData: any = {};
  public country:any=[];
  public wheather:any=[]
  constructor(private wdata:WheatherDataService) { 
    this.getWeatherData();
  }

  ngOnInit() {
    console.log(this.data);
  }
getWeatherData(){
  this.wdata.getData().subscribe((res:any) => {
      console.log(res);
      this.data =  res.list;
      
      console.log(this.data);
    });
}
}
