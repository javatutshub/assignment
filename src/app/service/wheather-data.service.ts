import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WheatherDataService {

  constructor(private http: HttpClient) { }

  getData() {
    let api_key = '2c5c3aba52b8bd0403b41ab29cde52de';
    return this.http.get<any>(`https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/2.5/find?lat=20.5937&lon=78.9629&cnt=30&appid=${api_key}`,

      {
        headers: {
          contenttype: "application/json",
          "Access-Control-Allow-Origin": "http://localhost:4200"
        },
      })

  }

  getUsData() {
    let api_key = '2c5c3aba52b8bd0403b41ab29cde52de';
    return this.http.get<any>(`https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/2.5/find?lat=42.933334&lon=76.566666&cnt=30&appid=${api_key}`,

      {
        headers: {
          contenttype: "application/json",
          "Access-Control-Allow-Origin": "http://localhost:4200"
        },
      })

  }
}
