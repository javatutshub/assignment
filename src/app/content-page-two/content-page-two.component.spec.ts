import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentPageTwoComponent } from './content-page-two.component';

describe('ContentPageTwoComponent', () => {
  let component: ContentPageTwoComponent;
  let fixture: ComponentFixture<ContentPageTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentPageTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentPageTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
