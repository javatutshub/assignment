import { Component, OnInit } from '@angular/core';
import { WheatherDataService } from '../service/wheather-data.service';

@Component({
  selector: 'app-content-page-two',
  templateUrl: './content-page-two.component.html',
  styleUrls: ['./content-page-two.component.css']
})
export class ContentPageTwoComponent implements OnInit {
  public data:any=[];
  cityData: any = {};
  public country:any=[];
  public wheather:any=[]
  constructor(private wdata:WheatherDataService) { 
    this.getWeatherData();
  }

  ngOnInit() {
    console.log(this.data);
  }
getWeatherData(){
  this.wdata.getUsData().subscribe((res:any) => {
      console.log(res);
      this.data =  res.list;
      
      console.log(this.data);
    });
}

}
